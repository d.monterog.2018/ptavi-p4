#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Programa cliente UDP que abre un socket a un servidor."""

import socket
import sys


# Constantes. Dirección IP del servidor y contenido a enviar y puerto
try:
    SERVER = str(sys.argv[1])  # IP del servidor al que nos conectamos
    PORT = int(sys.argv[2])  # Puerto del servidor al que nos conectamos
    Registro = str(sys.argv[3])
    Usuario = str(sys.argv[4])  # Pasamos el nombre del usuario
    Tiempo = int(sys.argv[5])  # Pasamos el tiempo de expiracion del usuario
    Direccion = Registro + " SIP:" + Usuario + " SIP/2.0" "\r\n" + "Expires:" + str(Tiempo) + "\r\n"

except IndexError:
    sys.exit("Usage: client.py ip puerto register sip_address expires_value")


# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.connect((SERVER, PORT))
    print("Enviando:", Direccion)
    my_socket.send(bytes(Direccion, 'utf-8') + b'\r\n')
    data = my_socket.recv(1024)
    print('Recibido -- ', data.decode('utf-8'))
    print()

print("Socket terminado.")
