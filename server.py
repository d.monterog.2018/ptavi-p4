#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Clase (y programa principal) para un servidor de eco en UDP simple."""

import socketserver
import sys
import json
from datetime import datetime, timedelta


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """Echo server class."""

    diccionario = {}

    def register2json(self):
        """Creacion del archivo json."""
        local = 'registered.json'
        with open(local, 'w') as jsonfile:  # Abrimos en modo escritura
            json.dump(self.diccionario, jsonfile, indent=3)

    def json2registered(self):
        """Leo el archivo json si existe."""
        local = 'registered.json'
        try:  # Intentamos buscar el archivo local, si esta lo leemos
            with open(local, 'r') as jsonfile:
                self.diccionario = json.load(jsonfile)
        except FileNotFoundError:  # Si no con el register2json creamos carpeta
            print("Se ha creado la carpeta registered.json automaticamente")

    def handle(self):
        """
        handle method of the server class.

        (all requests will be handled by this method)
        """
        if len(self.diccionario) == 0:
            self.json2registered()

        line = self.rfile.read()
        mensaje = line.decode('utf-8')
        ip = self.client_address[0]
        puerto = self.client_address[1]
        direccion = "IP: " + str(ip) + " Puerto:" + str(puerto)
        registro = mensaje.split(" ")[0]  # Separamos por espacios
        usuario = mensaje.split(" ")[1].split(":")[1]
        expira = mensaje.split(" ")[2].split("\r\n")[1].split(":")[1]
        address = "Address: " + ip
        formato = " %Y-%m-%d %H:%M:%S"
        sumadetiempo = datetime.now() + timedelta(seconds=int(expira))
        fecha = sumadetiempo.strftime(formato)  # Fecha del expire
        horacentral = datetime.now().strftime(formato)  # Fechalocal en formato

        if registro == 'register':  # Comprobamos si cinciden
            self.diccionario[usuario] = address, fecha  # Añade al diccionario

            if int(expira) == 0:  # Si pones 0, se borra
                print("Se ha eliminado el usuario: " + usuario)
                del self.diccionario[usuario]
                self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")

            for usuario in self.diccionario.copy():
                if horacentral >= self.diccionario[usuario][1]:
                    del self.diccionario[usuario]  # Borramos el usuario

            if int(expira) < 0:  # Si el expira es negativo nos salimos
                sys.exit("No se pueden utilizar valores negativos")

        print("\n" + direccion)
        self.register2json()
        print(self.diccionario)

        self.wfile.write(b"El cliente manda\r\n\r\n")
        for line in self.rfile:
            print("Hemos recibido ", line.decode('utf-8'))


if __name__ == "__main__":
    Puerto = int(sys.argv[1])

# Listens at localhost ('') port 6001
# and calls the EchoHandler class to manage the request
    serv = socketserver.UDPServer(('', Puerto), SIPRegisterHandler)
    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
